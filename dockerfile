FROM node:18-alpine
LABEL version="1.0" maintainer="LFT GROUP"
WORKDIR /
COPY public/ /public
COPY src/ /src
COPY package.json /
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]
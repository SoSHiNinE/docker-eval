# Docker eval

## Étape 1
git clone https://gitlab.com/SoSHiNinE/docker-eval.git

## Étape 2
étant donné que l'aplication React est déja créer, ainsi que le dockerfile qui permet de créer l'image et le container de l'app il suffit juste d'effectuer les commandes suivantes:

docker image build -t front-image:latest . : pour créer l'image
docker run -dp 8000:3000 --name front-container front-image:latest : pour créer le container et le lancer

## Étape 3
accéder a l'app via : http://localhost:8000

# Concenant la partie git
pour lancer les pipelines il y as 2 façons 

1. run une pipelines depuis l'interface gitlab
2. faire une modification et push sur une branche de l'app qui effectuera d'abord un test eslint sur l'app car tout ce passera sur la merge request puis une fois merge sur main il y aura un test d'installation puis un build de l'application qui permettra de créer des images de notre app avec des tag pour différencier les versions 

par exemple pour tester que l'image créer sur gitlab est opérationel on peut faire ceci:
docker run registry.gitlab.com/soshinine/docker-eval:v20240321135827